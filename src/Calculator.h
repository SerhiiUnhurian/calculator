
/*  Operator precedence
 *      *,/ - 3
 *      +,- - 2
 *      ( - 1
 *      ) - (-1)
 *      digit - 0
 * */

#ifndef CALCULATORFINAL_CALCULATOR_H
#define CALCULATORFINAL_CALCULATOR_H


#include <string>
#include <deque>

class Calculator {
private:
    std::deque<double> _savedResults;

    void saveResult(double result);
    double getSavedResult(int refIndex);

    int getPrecedence(char token);
    double perfomeOperation(double lVal, double rVal, char operation);
    double RPNtoAnswer(const std::string& rpn);
//    bool isOperator(char symbol);
    std::string expressionToRPN(const std::string& input);
public:
    double calculate(const std::string& input);
};


#endif //CALCULATORFINAL_CALCULATOR_H
