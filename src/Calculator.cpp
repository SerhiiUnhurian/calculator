#include <stack>
#include <sstream>
#include <set>
#include <iostream>
#include "Calculator.h"

int Calculator::getPrecedence(char token) {
    if (token == '*' || token == '/') return 3;
    else if (token == '+' || token == '-') return 2;
    else if (token == '(') return 1;
    else if (token == ')') return -1;
    else if (isdigit(token) || token == '.') return 0;
    else return -2;
}

void Calculator::saveResult(double result)
{
    if (_savedResults.size() == 3) {
        _savedResults.pop_back();
    }
    _savedResults.push_front(result);
}

double Calculator::getSavedResult(int refIndex)
{
    if (refIndex < 0 || refIndex >= _savedResults.size()) throw std::invalid_argument("Invalid reference!\n");

    return _savedResults[refIndex];
}

std::string Calculator::expressionToRPN(const std::string &input) {
    std::string current;
    std::stack<char> stack;
    int priority {};
    char symbol;

    for (int i = 0; i < input.length(); ++i) {
        symbol = input[i];
        priority = getPrecedence(symbol);

        if (symbol == ' ')
            continue;
        else if (symbol == '$') {
            std::string refIndex;
            if (getPrecedence(input[i+1]) != 0)
                throw std::invalid_argument("Invalid reference!\n");

            while (getPrecedence(input[++i]) == 0 && i < input.length()) {
                refIndex += input[i];
            }
            i--;
            double refVal = getSavedResult(std::stoi(refIndex));
            current += std::to_string(refVal);
        }
        else if (priority == 0)
            current += symbol;
        else if (priority == 1)
            stack.push(symbol);
        else if (priority > 1) {
            current += ' ';

            while (!stack.empty()) {
                if (getPrecedence(stack.top()) >= priority) {
                    current += stack.top();
                    stack.pop();
                } else {
                    break;
                }
            }
            stack.push(symbol);
        }
        else if (priority == -1) {
            while(getPrecedence(stack.top()) != 1) {
                current += stack.top();
                stack.pop();
            }
            stack.pop();
        }
        else {
            throw std::logic_error("Unsupported character!\n");
        }
    }
    while (!stack.empty()) {
        current += stack.top();
        stack.pop();
    }

    return current;
}

double Calculator::perfomeOperation(double lVal, double rVal, char operation) {
    switch (operation) {
        case '+':
            return lVal + rVal;
        case '-':
            return lVal - rVal;
        case '*':
            return lVal * rVal;
        case '/': {
            if (rVal == 0) {
                throw std::logic_error("Devision by zero!");
            }
            return lVal / rVal;
        }
    }
}

double Calculator::RPNtoAnswer(const std::string& rpn) {
    std::stack<double> stack;
    std::string operand;
    char symbol;

    for (int i = 0; i < rpn.length(); i++) {
        symbol = rpn[i];

        if (symbol == ' ')
            continue;
        else if (getPrecedence(symbol) == 0) {
            while (getPrecedence(symbol) == 0 && i < rpn.length()) {
                operand += symbol;
                symbol = rpn[++i];
            }
            i--;
            stack.push(std::stod(operand));
            operand.clear();
        }
        else if (getPrecedence(symbol) > 1) {
            double  rVal, lVal, result;

            rVal = stack.top();
            stack.pop();
            lVal = stack.top();
            stack.pop();

            result = perfomeOperation(lVal, rVal, symbol);
            stack.push(result);
        }
    }

    return stack.top();
}

double Calculator::calculate(const std::string &input) {
    std::string exp = expressionToRPN(input);
    std::cout << exp << std::endl;
    double result = RPNtoAnswer(exp);
    saveResult(result);

    return result;
}