#include <iostream>
#include "src/Calculator.h"
#include <set>
int main() {
    Calculator calc;
    std::string expression;

    std::cout << "Enter expression and press \"Enter\"\n";
    std::cout << "Press \"Q\" to quit.\n";
    std::cout << ">>";
    std::getline(std::cin, expression);

    while (expression != "Q") {
        if (expression == "") continue;

        try {
            std::cout << calc.calculate(expression) << std::endl;
        } catch (std::logic_error& ex) {
            std::cout << ex.what();
        }
        std::cout << ">>";
        std::getline(std::cin, expression);
    }

    return 0;
}
